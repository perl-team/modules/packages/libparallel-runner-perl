libparallel-runner-perl (0.014-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.014.
  * Update years of upstream copyright.
  * Update build dependencies.
  * Update upstream email address.
  * Update debian/upstream/metadata.
  * Remove spelling-errors.patch, applied upstream.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Thu, 09 May 2024 01:14:18 +0200

libparallel-runner-perl (0.013-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libparallel-runner-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 17:33:44 +0000

libparallel-runner-perl (0.013-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:31:56 +0100

libparallel-runner-perl (0.013-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 16:11:52 +0100

libparallel-runner-perl (0.013-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 May 2015 19:08:12 +0200

libparallel-runner-perl (0.013-1) unstable; urgency=low

  * Imported Upstream version 0.013
  * Update patch offset
  * Update Module::Build version dependency
  * Replace git.debian.org by anonscm.debian.org in Vcs fields

 -- Xavier Guimard <x.guimard@free.fr>  Sun, 12 May 2013 07:49:11 +0200

libparallel-runner-perl (0.012-1) unstable; urgency=low

  * Initial Release (Closes: #705256)

 -- Xavier Guimard <x.guimard@free.fr>  Sat, 13 Apr 2013 07:08:40 +0200
